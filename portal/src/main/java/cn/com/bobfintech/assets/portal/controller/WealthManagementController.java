package cn.com.bobfintech.assets.portal.controller;

import cn.com.bobfintech.assets.api.IForexService;
import cn.com.bobfintech.assets.api.IFundService;
import cn.com.bobfintech.assets.api.IMoneymanagementService;
import cn.com.bobfintech.assets.portal.config.NacosProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author wangjiaxing
 * @version 1.0
 * @date 2021/12/24 21:17
 */

@Slf4j
@RestController
@RequestMapping("/api/wealthManagement")
public class WealthManagementController {

    @Autowired
    private IForexService forexService;

    @Autowired
    private IFundService fundService;

    @Autowired
    private IMoneymanagementService moneymanagementService;

    @Autowired
    private NacosProperties nacosProperties;

    @GetMapping("/select/{id}")
    public String selectWealth(@PathVariable("id") Long id) {
        log.info("begin do wealthManagement");
        StringBuilder builder = new StringBuilder();
        log.info("nacos中获取的值: {}", nacosProperties.getFallbackSwitch());
        if (!nacosProperties.getFallbackSwitch()) {
            String forexInfo = forexService.selectForex(id);
            String fundInfo = fundService.select();
            String moneyInfo = moneymanagementService.select();

            builder.append(forexInfo);
            builder.append(fundInfo);
            builder.append(moneyInfo);
        } else {
            builder.append("服务降级");
        }
        return builder.toString();
    }

}
