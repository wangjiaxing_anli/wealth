package cn.com.bobfintech.assets.portal.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@Configuration
@RefreshScope
public class NacosProperties {

    @Value("${switch:false}")
    private boolean fallbackSwitch;

    public boolean getFallbackSwitch() {
        return fallbackSwitch;
    }

    public void setFallbackSwitch(boolean fallbackSwitch) {
        this.fallbackSwitch = fallbackSwitch;
    }
}
