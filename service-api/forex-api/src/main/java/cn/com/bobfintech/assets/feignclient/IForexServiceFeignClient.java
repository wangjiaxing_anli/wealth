package cn.com.bobfintech.assets.feignclient;

import cn.com.bobfintech.assets.api.IForexService;
import cn.com.bobfintech.assets.fallback.ForexServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "wealth-service-forex", configuration = FeignClientLogConfiguration.class, fallback = ForexServiceFallback.class)
public interface IForexServiceFeignClient extends IForexService {

}
