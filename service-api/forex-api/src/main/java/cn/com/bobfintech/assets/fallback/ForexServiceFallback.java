package cn.com.bobfintech.assets.fallback;

import cn.com.bobfintech.assets.feignclient.IForexServiceFeignClient;

public class ForexServiceFallback implements IForexServiceFeignClient {

    @Override
    public String selectForex(Long id) {
        return "查询外汇信息异常，Hystrix触发了降级保护机制";
    }
}
