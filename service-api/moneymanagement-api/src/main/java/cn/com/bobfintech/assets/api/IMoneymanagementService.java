package cn.com.bobfintech.assets.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/moneyManagement")
public interface IMoneymanagementService {

    @GetMapping("/select")
    String select();
}
