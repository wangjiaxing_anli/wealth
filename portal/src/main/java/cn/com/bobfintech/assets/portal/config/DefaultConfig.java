package cn.com.bobfintech.assets.portal.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author wangjiaxing
 * @version 1.0
 * @date 2021/12/24 21:18
 */

@Configuration
public class DefaultConfig {

    @LoadBalanced
    @Bean
    public RestTemplate getRestTemplate () {
        return new RestTemplate();
    }
}
