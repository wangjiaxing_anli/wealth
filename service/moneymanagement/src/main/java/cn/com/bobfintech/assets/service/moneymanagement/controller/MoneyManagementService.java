package cn.com.bobfintech.assets.service.moneymanagement.controller;

import cn.com.bobfintech.assets.api.IMoneymanagementService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjiaxing
 * @version 1.0
 * @date 2021/12/24 21:31
 */

@RestController
@RequestMapping("/api/moneyManagement")
public class MoneyManagementService implements IMoneymanagementService {

    @GetMapping("/select")
    public String select() {
        return "查询理财";
    }
}
