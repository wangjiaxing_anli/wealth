package cn.com.bobfintech.assets.service.forex.controller;

import cn.com.bobfintech.assets.api.IForexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjiaxing
 * @version 1.0
 * @date 2021/12/24 21:25
 */

@RestController
public class ForexManagementService implements IForexService {

    @GetMapping("/forex/select")
    public String selectForex(Long id) {
        if (id%2==0) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return "查询外汇";
    }
}
