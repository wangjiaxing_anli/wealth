package cn.com.bobfintech.assets.portal.config;

import cn.com.bobfintech.assets.fallback.ForexServiceFallback;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HystrixFallbackConfiguration {

    @Bean
    public ForexServiceFallback forexServiceFallback() {
        return new ForexServiceFallback();
    }
}
