package cn.com.bobfintech.assets.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface IForexService {

    @GetMapping("/forex/select")
    String selectForex(@RequestParam Long id);
}
