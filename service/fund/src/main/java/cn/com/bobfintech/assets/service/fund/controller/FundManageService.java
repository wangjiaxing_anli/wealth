package cn.com.bobfintech.assets.service.fund.controller;

import cn.com.bobfintech.assets.api.IFundService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjiaxing
 * @version 1.0
 * @date 2021/12/24 21:28
 */

@RestController
@RequestMapping("/api/fundMangement")
public class FundManageService implements IFundService {

    @GetMapping("/select")
    public String select() {
        return "查询基金";
    }
}
