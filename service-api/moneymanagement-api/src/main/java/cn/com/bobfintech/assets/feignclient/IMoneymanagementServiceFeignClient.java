package cn.com.bobfintech.assets.feignclient;

import cn.com.bobfintech.assets.api.IMoneymanagementService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "wealth-service-moneymanagement")
public interface IMoneymanagementServiceFeignClient extends IMoneymanagementService {

}
