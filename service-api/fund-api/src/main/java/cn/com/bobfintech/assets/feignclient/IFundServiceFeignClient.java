package cn.com.bobfintech.assets.feignclient;

import cn.com.bobfintech.assets.api.IFundService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "wealth-service-fund")
public interface IFundServiceFeignClient extends IFundService {

}
