package cn.com.bobfintech.assets.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/fundMangement")
public interface IFundService {

    @GetMapping("/select")
    String select();
}
